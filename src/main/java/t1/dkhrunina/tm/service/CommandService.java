package t1.dkhrunina.tm.service;

import t1.dkhrunina.tm.api.ICommandRepository;
import t1.dkhrunina.tm.api.ICommandService;
import t1.dkhrunina.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}