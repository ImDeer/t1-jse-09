package t1.dkhrunina.tm.repository;

import t1.dkhrunina.tm.api.ICommandRepository;
import t1.dkhrunina.tm.constant.ArgumentConst;
import t1.dkhrunina.tm.constant.CommandConst;
import t1.dkhrunina.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentConst.HELP, "Show available actions.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentConst.VERSION, "Show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentConst.ABOUT, "Show developer info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentConst.INFO, "Show system info.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Exit the application.");

    private static final Command[] COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}