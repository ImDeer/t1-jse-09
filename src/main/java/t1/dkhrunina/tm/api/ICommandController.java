package t1.dkhrunina.tm.api;

public interface ICommandController {

    void showArgumentError();

    void showSystemInfo();

    void showCommandError();

    void showVersion();

    void showAbout();

    void showHelp();

}