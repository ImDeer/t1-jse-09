package t1.dkhrunina.tm.component;

import t1.dkhrunina.tm.api.ICommandController;
import t1.dkhrunina.tm.api.ICommandRepository;
import t1.dkhrunina.tm.api.ICommandService;
import t1.dkhrunina.tm.constant.ArgumentConst;
import t1.dkhrunina.tm.constant.CommandConst;
import t1.dkhrunina.tm.controller.CommandController;
import t1.dkhrunina.tm.repository.CommandRepository;
import t1.dkhrunina.tm.service.CommandService;
import t1.dkhrunina.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommands() {
        System.out.println("*** Welcome to Task Manager ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nEnter command: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void processCommand(final String arg) {
        switch (arg) {
            case CommandConst.VERSION: {
                commandController.showVersion();
                break;
            }
            case CommandConst.ABOUT: {
                commandController.showAbout();
                break;
            }
            case CommandConst.HELP: {
                commandController.showHelp();
                break;
            }
            case CommandConst.INFO: {
                commandController.showSystemInfo();
                break;
            }
            case CommandConst.EXIT: {
                exit();
                break;
            }
            default: {
                commandController.showCommandError();
                break;
            }
        }
    }

    private void processArgument(final String arg) {
        switch (arg) {
            case ArgumentConst.VERSION: {
                commandController.showVersion();
                break;
            }
            case ArgumentConst.ABOUT: {
                commandController.showAbout();
                break;
            }
            case ArgumentConst.HELP: {
                commandController.showHelp();
                break;
            }
            case ArgumentConst.INFO: {
                commandController.showSystemInfo();
                break;
            }
            default: {
                commandController.showArgumentError();
                break;
            }
        }
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}