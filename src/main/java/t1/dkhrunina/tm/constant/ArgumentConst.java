package t1.dkhrunina.tm.constant;

public final class ArgumentConst {

    private ArgumentConst() {
    }

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    public static final String INFO = "-i";

}