package t1.dkhrunina.tm.controller;

import t1.dkhrunina.tm.api.ICommandController;
import t1.dkhrunina.tm.api.ICommandService;
import t1.dkhrunina.tm.model.Command;

import static t1.dkhrunina.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showArgumentError() {
        System.out.println("\n[ERROR]");
        System.err.println("Input arguments are not correct.");
        System.exit(1);
    }

    @Override
    public void showSystemInfo() {
        System.out.println("\n[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("Processors: " + processorCount);
        System.out.println("Max memory: " + formatBytes(maxMemory));
        System.out.println("Total memory: " + formatBytes(totalMemory));
        System.out.println("Free memory: " + formatBytes(freeMemory));
        System.out.println("Used memory: " + formatBytes(usedMemory));
    }

    @Override
    public void showCommandError() {
        System.out.println("\n[ERROR]");
        System.err.println("Command is not correct.");
    }

    @Override
    public void showVersion() {
        System.out.println("\n[VERSION]");
        System.out.println("1.9.0");
    }

    @Override
    public void showAbout() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Daria Khrunina");
        System.out.println("Email: dkhrunina@t1-consulting.ru");
    }

    @Override
    public void showHelp() {
        System.out.println("\n[HELP]");
        for (final Command command : commandService.getCommands()) System.out.println(command);
    }

}