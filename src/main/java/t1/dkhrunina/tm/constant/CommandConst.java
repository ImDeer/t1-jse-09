package t1.dkhrunina.tm.constant;

public final class CommandConst {

    private CommandConst() {
    }

    public static final String HELP = "Help";

    public static final String VERSION = "Version";

    public static final String ABOUT = "About";

    public static final String INFO = "Info";

    public static final String EXIT = "Exit";

}